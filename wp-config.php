<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_juke');

/** MySQL database username */
define('DB_USER', 'test');

/** MySQL database password */
define('DB_PASSWORD', 'test');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|+jf.hp{OnE^!-FO8#dO{[nw0,ou*><#>8gkt-Ge,QQ[(tbB-A.*W6709rF:B~yf');
define('SECURE_AUTH_KEY',  'cPfK.2=p6+P|3rFMt)2Lg@56gQ ^qrtPBS~HjLu9|rs$90[g;ZG1Ip8ZiiM{Wm6E');
define('LOGGED_IN_KEY',    'Lr2!XmT%wY?T`Z$o)z4lZp^h|wE!zqD(D~nb&d.R3AOFXu^W@Lu(r2jC>-V5Xda%');
define('NONCE_KEY',        'ojK6LRmUIGJN HQ-_1w_hrXkPi}ZP&iE!?Q,2BUW|v%3mXM(-!..KBaVx~GU-j?J');
define('AUTH_SALT',        'PKV^,mS[VS|:=3*q@m_1$#=((2Ank=Yd5%BMHeOx6n:DXu0-4K=@{ikWHa5cl{V$');
define('SECURE_AUTH_SALT', 'zw8h16%u7OAR2-b@v,_Ws:Qz)=@&!_0iaB5eu3TKkT4@6%bd`IyKBTmhNMM]Qh}<');
define('LOGGED_IN_SALT',   '<5s<iljkvhli~oQ))F9+pwbkoF7K(|wcP89sLq2&B-+5XnPE+;#DB61:#s,7lIC>');
define('NONCE_SALT',       'gxsgmFJs$,q^JzSvJ-JG}VMiTq]Lo%T C;}C-%gD! @W5R054&-A}vq ,KLqUDPO');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
